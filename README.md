# F8 End to End (e2e) testing utils

## Install with npm
```bash
npm install --save @dreamtsoft/test
````
Use module
```js
import { F8Select } from '@dreamtsoft/test';
```

## Documentation
Good place to start is [overview](https://gitlab.com/dreamtsoft/test/blob/master/docs/guide.md) and list of guides [here](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md).

## Examples

Some test examples are located in our [example bundle](https://gitlab.com/dreamtsoft/ds.example).

