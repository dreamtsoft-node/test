# Configuration

Here are the environment variables you can pass in at runtime to configure how your tests are ran.

```bash
# Default values are present
#
# Target base url to run tests against
BASE_URL=http://localhost:8080
# username to login as
E2E_USER=admin
# password for the username
E2E_PASS=admin


# Changes the the base url the tests are ran against
BASE_URL=https://myawesomesite.com npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/awesome-test.e2e.ts

# Change the user you are logging in as
E2E_USER=my-username-here E2E_PASS=mypassword npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/awesome-test.e2e.ts

# Work in a certain space
SPACE_ID=it_dev npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/awesome-test.e2e.ts

# When the SpaceId is specified the F8Utils.URL() builds the url with the targeted SpaceId in it
# Also note by default F8Utils.loginUser() uses F8Utils.URL('/') by default so the login page would
# be the subspace login page in when configured


# Lets say we want to work in subspace but login as a top user
FORCE_LOGIN_PATH=/ SPACE_ID=it_dev npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/awesome-test.e2e.ts

# So we can pass in a path to FORCE_LOGIN_PATH then it will instruct all F8Utils.loginUser() calls
# to use that path instead of trying to login in the subspace auth page
```

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
