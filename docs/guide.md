
# Guide

This library builds on top of [TestCafe](https://devexpress.github.io/testcafe/) providing out of the box helpers and utilities to help End to End test your [Dreamsoft](https://www.dreamtsoft.website/) application.

Here are some relevant links on the testing framework

* [Test API ](https://devexpress.github.io/testcafe/documentation/test-api/)
* [Command line interface](https://devexpress.github.io/testcafe/documentation/using-testcafe/command-line-interface.html)

## Basic Usage

##### Ex. Selecting a component and prove that exits on the page

* Lets create a test file in tests/e2e/features/my-awesome-feature/first-test.e2e.ts
* Also the note the extension has 'e2e' and is a TypeScript file. We search for this extension when running the tests

```js
// first-test.e2e.ts
import { F8Select, F8Utils } from '@dreamtsoft/test';
const { URL } = F8Utils;

fixture `My name for these group of tests`
    .page(URL(`/`)); // Lets go to the login page

test('My test', async t => {
    const authComponent = F8Select().component('auth').get();
    await t
        .expect(authComponent.exists))
        .eql(true);
});
````

Now lets run this test via one of these methods

Running a specific test against chrome
```bash
npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/first-test.e2e.ts
```

Please note all requests run against the default localhost setup which is http://localhost:8080. To change this behavior use environment variables and set BASE_URL

```bash
BASE_URL=http://localhost:8081 npx testcafe 'chrome' tests/e2e/features/my-awesome-feature/first-test.e2e.ts
```

More on how you configure the run time via environment variables can be found [here](configuration.md)

##### Ex. Lets login prove the login works
* Note that we are now selecting 3 things IN the 'auth' component
* If we login then we can see the bundles component on the page
```js
import { F8Select, F8Utils } from '@dreamtsoft/test';
const { URL } = F8Utils;

const username = 'admin';
const password = 'admin';

fixture `My name for these group of tests`
    .page(URL(`/`)); // Lets go to the login page

test('My test', async t => {
    const usernameInput = F8Select().component('auth', 'login-user-input').get();
    const passwordInput = F8Select().component('auth', 'login-pass-input').get();
    const loginBtn = F8Select().component('auth', 'login-btn').get();
    const bundlesComponent = F8Select().component('bundles').get();

    await t
        .typeText(usernameInput, username)
        .typeText(passwordInput, password)
        .click(loginBtn)
        .expect(bundlesComponent.exists)
            .eql(true);
});
```

### Whats Next?
* More info on selecting elements in the F8 platform can be found here [F8Select](https://gitlab.com/dreamtsoft/test/blob/master/docs/selecting.md)
* To discover more commands to interact with the browser or the assertion library go here [Test API ](https://devexpress.github.io/testcafe/documentation/test-api/)


[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
