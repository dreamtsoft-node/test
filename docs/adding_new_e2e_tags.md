
# Adding E2E Tags

#### Creating tags
We have utility function that helps with creating the E2E tag.

```js
// A tag any where
E2ESelector(tagLabel)

// A tag with a namespace
// Good example would be a component or widget E2E tag
// E2ESelector(namespace, tagLabel)
// Examples:

// Component namespace would be the component name and then whatever your new label is
E2ESelector('calendar', 'add-event').toString()

// Widget namespace would be the widget name and then whatever your new label is
E2ESelector('slot_picker', 'select-btn').toString()

````

#### Adding tags to the DOM

```html
// In our F8 templates your setup a tag like this
// Please note you need to add the data attribute 'data-e2e'
<div data-e2e="<%= E2ESelector('slot_picker', 'select-btn').toString() %>">
    ...
</div>
```

```html
// In react.js you can use the toObject() method to spread it on to an element
<button
	onClick={props.onNewRecord}
	{...E2ESelector('slot_picker', 'select-btn').toObject()}
	className="btn" />
```

```html
// In vue.js
{
    template: `
        <div>
            <my-awesome-component :data-e2e="e2e()" />
        </div>
    `,

    methods: {
        e2e() {
            return E2ESelector('slot_picker', 'select-btn').toString();
        },
    }
}
```

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
