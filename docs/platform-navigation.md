# Platform Navigation Selecting

```js
////////////////////////////////
/// Bundles Menu
////////////////////////////////

// Link to open menu
F8Select().nav().bundles().menu().get();
// Nav drawer opened selectors
F8Select().nav().bundles().settings().get(); // Bundles settings link

// Bundles links by bundleId
// Ex. System link in bundle pull out
F8Select().nav().bundles().link('ds.base').get();

////////////////////////////////
/// Bundle navigation menu
////////////////////////////////

// Link to open menu
F8Select().nav().bundleNav().menu().get();
// Nav drawer opened selectors
F8Select().nav().bundleNav().home().get(); // home link
F8Select().nav().bundleNav().link('general').get(); // pass in link name

////////////////////////////////
/// Bundle config menu
////////////////////////////////

// Link to open menu
F8Select().nav().bundleConfig().menu().get();
// Nav drawer opened selectors
// Links in bundle config menu grab by name
F8Select().nav().bundleConfig().link('information').get();
F8Select().nav().bundleConfig().link('files').get();
// ...etc

////////////////////////////////
/// Customize menu
////////////////////////////////

// Link to open menu
F8Select().nav().customize().menu().get();
// Nav drawer opened selectors
F8Select().nav().customize().pageEdit().get();
F8Select().nav().customize().componentEdit('details').get(); // Specify name of component

////////////////////////////////
///  Search menu
////////////////////////////////

// Link to open menu
F8Select().nav().search().menu().get();
// Nav drawer opened selectors
F8Select().nav().search().input().get();  // Search input field
F8Select().nav().search().firstResult().get();  // First search result
F8Select().nav().search().results().get();  // Select all results

////////////////////////////////
/// Space tree menu
////////////////////////////////

// Link to space tree
F8Select().nav().spaceTree().menu().get();

////////////////////////////////
/// User menu
////////////////////////////////

// Link to open menu
F8Select().nav().user().menu().get();
// Nav drawer opened selectors
F8Select().nav().user().logout().get();

````

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
