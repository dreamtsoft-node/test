# List Component

```js
// To access the List object call list()
F8Select().list().rowHeader().get();

// Get all rows
F8Select().list().rows().get();

// Get first row by passing in a css pseudo class into rows
F8Select().list().rows('first-child').get();

// Check box for a row
F8Select().list().rows('first-child').checkbox().get();

// Link to the record
F8Select().list().rows('first-child').recordLink().get();

// Select row record by record ID
F8Select().list().rowsByRecordId('13AFALDADn754DADFAWEDL34D90FMV4').recordLink().get();

````

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
