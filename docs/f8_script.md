# F8 Scripts

You are able to run F8Scripts from your tests. These would be typically used for seeding data into your test. They are most of the time used in your before/after test hooks.

## Basic

```js
// Import the util function
import { F8Script } from '@dreamtsoft/test';

// Setup the caller
const f8script = F8Script();

fixture `My Awesome Test`
    .page(URL('/'))
    .beforeEach(async t => {

        // This looks for a file called some_awesome_seed_script.before.js in ./__f8scripts__ directory (Please not this folder is in the same folder as your test)
        // runs (./__f8scripts__/some_awesome_seed_script.before.js)
        // __f8scripts__ is a special directory where it looks for F8 Scripts to run
        // Calls a file some_awesome_seed_script.
        await f8script.runBefore(t, 'some_awesome_seed_script');
    })
    .afterEach(async () => {
        // This looks for a file called some_awesome_seed_script.after.js in ./__f8scripts__ directory (Please not this folder is in the same folder as your test)
        // runs (./__f8scripts__/some_awesome_seed_script.after.js)
        await f8script.runAfter(t, 'some_awesome_seed_script');
    });
````

```js
// some_awesome_seed_script.before.js

// File contains regular F8Script

var fr = new FRecord('bundle_navigation');
fr.addSearch('name', 'e2eTest');
fr.search(function (item) {
    item.del();
});
```

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
