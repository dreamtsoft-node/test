# F8 Actions

Actions appear in the form of a button through various parts of the UI. Most notably appearing in the details and list components on top.

Managing these can be found here (Platform Nav > Bundle Config > Bucket Mangement > Actions). Once in the details page of a Browser action there is a button tab where a field is labeled 'Button E2E Tag', this value is use to target buttons in the example below

## Basic

```js
// Select an action tag by the e2e tag value
F8Select().action('add-new-record').get();
````

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
