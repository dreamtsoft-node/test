# Dialog Selecting

```js
// To access the dialog methods

// Selector for dialog action pass in Label formatted by this formula'
// label argument = (lowercased, special characters removed, blank spaces replaced with dashes )
F8Select().dialogAction('ok').get();
F8Select().dialogAction('click-me').get();
F8Select().dialogAction('close').get();

// Grab the body of dialog
F8Select().inDialog().get();

// Grab a component in a dialog
F8Select().inDialog().component('details').get();
````

[Back](https://gitlab.com/dreamtsoft/test/blob/master/docs/index.md)
