import { Selector, ClientFunction } from 'testcafe';
import { URL } from './F8Utils';
import F8Select from './F8Select';

export default class F8SpaceTree {
    public static subSpaceName = 'testspacee2e';

    public static navigateToBaseSpaces(t: TestController): Promise<TestController> {
        return t.navigateTo(URL('/ds.base/spaces'));
    }

    public static async backToHomeSpace(t: TestController): Promise<TestController> {
        const goToHomeSpace = ClientFunction(() => {
            const Configuration = require('bootstrap/Configuration');
            const st = Configuration.getSpaceTreeClass();
            window.location.href = `/s/${st.getTopSpace().name}`;
        });

        await goToHomeSpace();
        return t;
    }

    public static selectSpaceTreeBoxRelativeToName(t, name?: string) {
        return Selector('div', { boundTestRun: t })
            .withExactText(name ? name : F8SpaceTree.subSpaceName)
            .parent()
            .parent()
            .parent();
    }

    public static createSubSpace(t: TestController, name?: string): Promise<TestController> {
        const lastDialogBox = F8Select(t)
            .add('f8dialog')
            .get()
            .nth(-1);
        const firstSpaceTreeBox = F8Select(t)
            .component('space_tree', 'space-tree-box:first-child', 'box-actions')
            .get();
        const firstSpacTreeBoxCreate = F8Select(t)
            .component('space_tree', 'space-tree-box:first-child', 'box-actions', 'action-create')
            .get();
        const spaceTreeBoxNameInputField = F8Select(t)
            .inDialog()
            .widget('name', 'string', 'input')
            .getString();
        const createSpaceTreeBtn = F8Select(t)
            .dialogAction('ok')
            .getString();
        return t
            .click(firstSpaceTreeBox)
            .click(firstSpacTreeBoxCreate)
            .typeText(lastDialogBox.find(spaceTreeBoxNameInputField), name ? name : F8SpaceTree.subSpaceName)
            .click(lastDialogBox.find(createSpaceTreeBtn));
    }

    public static workInSubSpace(t: TestController, name?: string): Promise<TestController> {
        const spaceTreeBox = F8SpaceTree.selectSpaceTreeBoxRelativeToName(t, name);
        const boxAction = spaceTreeBox.find(
            F8Select(t)
                .component('space_tree', 'box-actions')
                .getString(),
        );
        const boxActionSwitchto = spaceTreeBox.find(
            F8Select(t)
                .component('space_tree', 'box-actions', 'action-switchto')
                .getString(),
        );
        return t.click(boxAction).click(boxActionSwitchto);
    }

    public static tearDownSubSpace(t: TestController, name?: string): Promise<TestController> {
        const lastDialogBox = F8Select(t)
            .add('f8dialog')
            .get()
            .nth(-1);
        const spaceTreeBox = F8SpaceTree.selectSpaceTreeBoxRelativeToName(t, name);
        const boxAction = spaceTreeBox.find(
            F8Select(t)
                .component('space_tree', 'box-actions')
                .getString(),
        );
        const boxActionDelete = spaceTreeBox.find(
            F8Select(t)
                .component('space_tree', 'box-actions', 'action-delete')
                .getString(),
        );
        const confirmDeleteInput = F8Select(t)
            .inDialog()
            .widget('delete', 'string', 'input')
            .getString();
        const confirmDeleteBtn = F8Select(t)
            .dialogAction('ok')
            .getString();

        return t
            .click(boxAction)
            .click(boxActionDelete)
            .typeText(lastDialogBox.find(confirmDeleteInput), 'Delete')
            .click(lastDialogBox.find(confirmDeleteBtn));
    }

    public static async setupAndSwitchToSubSpaceToWorkIn(t: TestController, name?: string): Promise<void> {
        await F8SpaceTree.navigateToBaseSpaces(t);
        await F8SpaceTree.createSubSpace(t, name ? name : F8SpaceTree.subSpaceName);
        await F8SpaceTree.workInSubSpace(t, name ? name : F8SpaceTree.subSpaceName);
    }

    public static async switchOutOfTearDownSubspace(t: TestController, name?: string): Promise<void> {
        await F8SpaceTree.backToHomeSpace(t);
        await F8SpaceTree.navigateToBaseSpaces(t);
        await F8SpaceTree.tearDownSubSpace(t, name ? name : F8SpaceTree.subSpaceName);
    }
}
