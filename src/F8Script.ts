import { ClientFunction, Selector } from 'testcafe';
import { URL, loginCredentials } from './F8Utils';
import axios from 'axios';
import * as path from 'path';
import * as fs from 'fs';

export default () => {
    const stack = getStack();
    stack.shift();
    const callerFile = stack[0].getFileName();
    const callerDir = path.dirname(callerFile);

    return {
        runBefore: async (t: TestController, fileId: string, debug = false, sameSession = false) => {
            await run(t, callerDir, fileId, 'before', debug, sameSession);
        },
        runAfter: async (t: TestController, fileId: string, debug = false, sameSession = false) => {
            await run(t, callerDir, fileId, 'after', debug, sameSession);
        },
        run: async (t: TestController, fileId: string, debug = false, sameSession = false) => {
            await run(t, callerDir, fileId, '', debug, sameSession);
        },
    };
};

async function run(t: TestController, callerDir: string, fileId: string, fileSuffix: string, debug = false, sameSession = false) {
    const fileName = fileId + (fileSuffix ? `.${fileSuffix}` : '') + '.js';

    let files = await readfilesFromF8Scripts(callerDir);
    files = files.filter(f => f.endsWith(`${fileName}`));
    if (files.length === 0) {
        // tslint:disable-next-line
        console.warn(`No F8Scripts found for ${fileId}`);
        return;
    }
    const file = files[0];
    const code = await readfileContents(file);

    if (sameSession) {
        await runF8ScriptSameSession(t, code.toString());
    } else {
        await runF8Script(code.toString(), debug);
    }
}

function readfilesFromF8Scripts(basePath: string): Promise<string[]> {
    const f8scriptPath = `${basePath}/__f8scripts__`;
    return new Promise((accept, reject) => {
        fs.readdir(f8scriptPath, (err, files) => {
            if (err) {
                // tslint:disable-next-line
                return console.error('Unable to scan directory: ' + err);
            }
            accept(files.map(file => `${basePath}/__f8scripts__/${file}`));
        });
    });
}

function readfileContents(file: string): Promise<Buffer> {
    return new Promise((accept, reject) => {
        fs.readFile(file, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            accept(data);
        });
    });
}

async function runF8ScriptSameSession(t: TestController, code): Promise<any> {
    const ajaxCompleteNotifySuccess = Selector('div[data-e2e="notify e2e-script-catch-success2"]');

    const mainScriptToRun = ClientFunction(codeBlock => {
        const comp = new (window as any).Component('scripter');
        comp.ajax(
            'testSupportRunScript',
            { code: codeBlock },
            {
                callback: () => {
                    const F8Notify = (window as any).require('core/utils/notifications/Notify');
                    F8Notify.Notify.send('E2E F8Script.runF8ScriptSameSession', 'e2e-script-catch-success');
                },
            },
        );
    }).with({ boundTestRun: t });

    const options = {
        dependencies: { ajaxCompleteNotifySuccess },
        boundTestRun: t,
    };
    // tslint:disable-next-line
    const catchScriptComplete = ClientFunction(() => {}, options);

    // One promise runs the script then other promise will
    // wait til the E2E catch success notification is displayed
    await Promise.all([catchScriptComplete(), mainScriptToRun(code)]);
}

async function runF8Script(code, debug = false): Promise<any> {
    const scriptApiUrl = URL('/b/ds.base/component/scripter/ajax/testSupportRunScript');
    const params = {
        code,
        bundle: 'ds.base',
    };
    const preparedParams = JSON.stringify(params);

    const username = loginCredentials().username;
    const password = loginCredentials().password;

    try {
        const response: any = await axios({
            method: 'POST',
            url: scriptApiUrl,
            data: `variables=${encodeURIComponent(preparedParams)}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            auth: {
                username,
                password,
            },
        });

        const { status, statusText, data } = response;
        const { logLines } = response.data;

        //   System Level  or Application Level
        if (status !== 200 || data.status !== 'good') {
            if (data.data.message) {
                throw Error(`F8Script failed to run. Reason [${data.data.message}]`);
            }

            const message = statusText ? statusText : 'Unknown';
            throw Error(`F8Script failed to run. Reason [${message}]`);
        }

        if (debug) {
            logLines.forEach(log => {
                // tslint:disable-next-line
                console.log(log);
            });
        }

        return data;
    } catch (error) {
        // tslint:disable-next-line
        console.error(error);
    }

    return {};
}

function getStack(): any {
    // Save original Error.prepareStackTrace
    const origPrepareStackTrace = Error.prepareStackTrace;

    // Override with function that just returns `stack`
    Error.prepareStackTrace = (_, errStack) => {
        return errStack;
    };

    // Create a new `Error`, which automatically gets `stack`
    const err = new Error();

    // Evaluate `err.stack`, which calls our new `Error.prepareStackTrace`
    const stack: any = err.stack;

    // Restore original `Error.prepareStackTrace`
    Error.prepareStackTrace = origPrepareStackTrace;

    // Remove superfluous function call on stack
    const shifted = stack.shift(); // getStack --> Error

    return stack;
}
