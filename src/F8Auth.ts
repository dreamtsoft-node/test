import F8Select from './F8Select';

export default class F8Auth {
    public static async login(t: TestController, username: string, password: string, loginUrlFinal: string) {
        const usernameInput = F8Select(t)
            .component('auth', 'login-user-input')
            .get();
        const passwordInput = F8Select(t)
            .component('auth', 'login-pass-input')
            .get();
        const termsInput = F8Select(t)
            .component('auth', 'login-terms-box')
            .get();

        const loginBtn = F8Select(t)
            .component('auth', 'login-btn')
            .get();

        await t.navigateTo(loginUrlFinal);
        await t.typeText(usernameInput, username).typeText(passwordInput, password);

        if (await termsInput.exists) {
            await t.click(termsInput);
        }

        await t.click(loginBtn);
    }

    public static async logout(t: TestController) {
        const userMenu = F8Select(t)
            .nav()
            .user()
            .profile()
            .get();
        const logoutBtn = F8Select(t)
            .nav()
            .user()
            .logout()
            .get();
        await t.click(userMenu).click(logoutBtn);
    }
}
