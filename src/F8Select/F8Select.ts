import { Selector } from 'testcafe';
import htmlTags from './html-tags';

type STARTS_WITH = '^=';
type EQUALS = '=';

export const SELECTOR_OPERATORS: {
    STARTS_WITH: STARTS_WITH;
    EQUALS: EQUALS;
} = {
    STARTS_WITH: '^=',
    EQUALS: '=',
};

export default class F8Select {
    private selectors: string[] = [];
    private lastNamespace: string = '';
    private debugFlag = false;
    private testController?: TestController;

    constructor(t?: TestController) {
        if (t) {
            this.testController = t;
        }
    }

    public component(...selectors: string[]) {
        const componentName = selectors.shift();

        if (componentName === undefined) {
            throw Error('First argument required for component(componentName, tags...)');
        }

        this.addSelector(`component-${componentName}`);
        this.setNamespace(componentName);
        const query = this.queryPath(componentName);
        selectors.forEach(sel => {
            query.add(sel);
        });
        return this;
    }

    public widget(...selectors: string[]) {
        const slotName = selectors.shift();
        const slotType = selectors.shift();

        if (slotName === undefined || slotType === undefined) {
            throw Error('Arguments required for widget(slotName, slotType, tags...)');
        }

        this.addSelector(`widget-manager`);
        this.addSelector(`widget-${slotName}`);
        this.setNamespace(slotType);
        const query = this.queryPath(slotType);
        selectors.forEach(sel => {
            query.add(sel);
        });
        return this;
    }

    public slideIn() {
        this.queryPath('slide_in').add('panel:last-child');
        return this;
    }

    public slideInAction(e2eTag: string) {
        this.queryPath('slide_in')
            .add('panel:last-child')
            .add(`action-${e2eTag}`);
        return this.finishedSelectors();
    }

    public action(e2eTag: string) {
        this.add(`f8action-${e2eTag}`);
        return this.finishedSelectors();
    }

    public list(): IListSelectors {
        const listChainReturnObj = {
            ...this.publicSelectors(),
            checkbox: () => {
                this.addSelector(`list checkbox`);
                return this.finishedSelectors();
            },
            recordLink: () => {
                this.addSelector(`list record-link`);
                return this.finishedSelectors();
            },
        };

        return {
            rowHeader: () => {
                return this.component('list', 'header-row');
            },
            rowsByRecordId: (id: string) => {
                this.component('list');
                this.addSelector(`list row ${id}`);
                return listChainReturnObj;
            },
            rows: (pseudoClassSelector?: string) => {
                this.component('list');
                const tag = pseudoClassSelector ? `list row:${pseudoClassSelector}` : 'list row';
                this.addSelector(tag, SELECTOR_OPERATORS.STARTS_WITH);
                return listChainReturnObj;
            },
        };
    }

    public nav(): INavSelectors {
        return {
            bundles: () => {
                return {
                    menu: () => this.component('left_bar', 'bundles-menu').finishedSelectors(),
                    settings: () => this.component('left_bar', 'manage-bundles').finishedSelectors(),
                    link: (bundleId: string) => this.component('left_bar', `bundles bundle-link ${bundleId}`).finishedSelectors(),
                };
            },
            bundleNav: () => {
                return {
                    link: (name: string) => this.component('left_bar', `bundle-menu-link ${name}`).finishedSelectors(),
                };
            },
            bundleConfig: () => {
                return {
                    menu: () => this.component('left_bar', 'bundle-config-menu').finishedSelectors(),
                    link: (id: string) => this.component('left_bar', `bundle-menu-link ${id}`).finishedSelectors(),
                };
            },
            user: () => {
                return {
                    profile: () => this.component('user_profile', 'open').finishedSelectors(),
                    logout: () => this.slideInAction('logout-btn'),
                };
            },
            search: () => {
                return {
                    menu: () => this.component('banner_side', 'search-menu').finishedSelectors(),
                    input: () => this.component('banner_side', 'global-search-input').finishedSelectors(),
                    firstResult: () => this.component('banner_side', 'global-search-item:first-child').finishedSelectors(),
                    results: () => this.component('banner_side', 'global-search-item').finishedSelectors(),
                };
            },
            spaceTree: () => {
                return {
                    menu: () => this.component('banner_side', 'spacetree-menu').finishedSelectors(),
                };
            },
            customize: () => {
                return {
                    menu: () => this.component('left_bar', 'customize-menu').finishedSelectors(),
                    pageEdit: () => this.component('left_bar', 'page-edit-link').finishedSelectors(),
                    componentEdit: name => this.component('left_bar', `component-edit-link ${name}`).finishedSelectors(),
                };
            },
        };
    }

    public debug() {
        this.debugFlag = true;
        return this;
    }

    public loading() {
        this.queryPath('f8dialog').add('loading');
        return this.finishedSelectors();
    }

    public inDialog() {
        this.addSelector('f8dialog');
        this.queryPath('f8dialog').add('content');
        return this;
    }

    public dialogAction(e2eTag: string) {
        this.queryPath('f8dialog').add(e2eTag);
        return this.finishedSelectors();
    }

    public notification(): INotificationSelectors {
        return {
            success: () => {
                this.queryPath('notify').add('success');
                return this.finishedSelectors();
            },
            failed: () => {
                this.queryPath('notify').add('danger');
                return this.finishedSelectors();
            },
            warned: () => {
                this.queryPath('notify').add('warning');
                return this.finishedSelectors();
            },
            info: () => {
                this.queryPath('notify').add('info');
                return this.finishedSelectors();
            },
        };
    }

    public add(...selectors: string[]) {
        const query = this.queryPath();
        selectors.forEach(selector => {
            query.add(selector);
        });
        return this;
    }

    public get(options?: SelectorOptions) {
        return this.serialize(options);
    }

    public getString(options?: SelectorOptions) {
        return this.serializeString(options);
    }

    private getNamespace() {
        return this.lastNamespace;
    }

    private setNamespace(label: string) {
        this.lastNamespace = label;
    }

    private publicSelectors(): IPublicSelectors {
        return {
            component: this.component.bind(this),
            widget: this.widget.bind(this),
            action: this.action.bind(this),
            list: this.list.bind(this),
            nav: this.nav.bind(this),
            loading: this.loading.bind(this),
            inDialog: this.inDialog.bind(this),
            dialogAction: this.dialogAction.bind(this),
            notification: this.notification.bind(this),
            add: this.add.bind(this),
            get: this.get.bind(this),
            getString: this.getString.bind(this),
        };
    }

    private finishedSelectors(): IFinishSelectors {
        return {
            get: this.get.bind(this),
            getString: this.getString.bind(this),
        };
    }

    private queryPath(namespace?: string): IQueryPathReturn {
        this.setNamespace(namespace ? namespace : '');
        return {
            add: (...selectors: string[]) => {
                selectors.forEach(selector => {
                    if (this.isRawHTMLSelector(selector)) {
                        this.addSelector(selector);
                        return;
                    }
                    const sel = this.getNamespace() ? `${this.getNamespace()} ${selector}` : selector;
                    this.addSelector(sel);
                });
                return this.getNamespace() ? this.queryPath(this.getNamespace()) : this.queryPath();
            },
            get: this.get.bind(this),
            getString: this.getString.bind(this),
        };
    }

    private getSelectors(): string[] {
        return this.selectors.map(x => x);
    }

    private serialize(options: SelectorOptions = {}): Selector {
        const serialized = this.getSelectors().join(' ');
        if (this.debugFlag) {
            // tslint:disable-next-line
            console.log(`CSS SELECTOR: ${serialized}`);
        }

        if (this.testController) {
            const optionsWithBoundedT = Object.assign(options, { boundTestRun: this.testController });
            return Selector(serialized, optionsWithBoundedT);
        }

        return Selector(serialized, options);
    }

    private serializeString(options?: SelectorOptions): string {
        const serialized = this.getSelectors().join(' ');
        if (this.debugFlag) {
            // tslint:disable-next-line
            console.log(`CSS SELECTOR: ${serialized}`);
        }
        return serialized;
    }

    private addSelector(selector: string, operator: EQUALS | STARTS_WITH = SELECTOR_OPERATORS.EQUALS) {
        if (this.isRawHTMLSelector(selector)) {
            this.rawSelector(selector);
            return;
        }
        // if css modifier attached then pass to selector
        if (selector.indexOf(':') > -1) {
            const tmp = selector.split(':');
            this.selectors.push(`[data-e2e${operator}"${tmp[0]}"]:${tmp[1]}`);
            return;
        }
        this.selectors.push(`[data-e2e${operator}"${selector}"]`);
    }

    private rawSelector(selector: string) {
        this.selectors.push(selector);
    }

    private isRawHTMLSelector(selector: string) {
        if (selector.indexOf(':') > -1) {
            const tmp = selector.split(':');
            return htmlTags.indexOf(tmp[0]) > -1;
        }
        return htmlTags.indexOf(selector) > -1;
    }
}

interface IPublicSelectors {
    component(...selectors: string[]): F8Select;
    widget(...selectors: string[]): F8Select;
    action(e2eTag: string): IFinishSelectors;
    list(): IListSelectors;
    nav(): INavSelectors;
    loading(): IFinishSelectors;
    inDialog(): F8Select;
    dialogAction(e2eTag: string): IFinishSelectors;
    notification(): INotificationSelectors;
    add(...selectors: string[]): F8Select;
    get(options?: SelectorOptions): Selector;
    getString(options?: SelectorOptions): string;
}

type IQueryPathReturn = IFinishSelectors & {
    add: (...selectors: string[]) => IQueryPathReturn;
};

interface INotificationSelectors {
    success: () => IFinishSelectors;
    failed: () => IFinishSelectors;
    warned: () => IFinishSelectors;
    info: () => IFinishSelectors;
}

interface IListSelectors {
    rowHeader: () => F8Select;
    rowsByRecordId: (id: string) => IListSelectors2ndLvl;
    rows: (pseudoClassSelector?: string) => IListSelectors2ndLvl;
}

interface IListSelectors2ndLvl extends IPublicSelectors {
    checkbox: () => IFinishSelectors;
    recordLink: () => IFinishSelectors;
}

interface INavSelectors {
    bundles: () => {
        menu: () => IFinishSelectors;
        settings: () => IFinishSelectors;
        link: (bundleId: string) => IFinishSelectors;
    };
    bundleNav: () => {
        link: (name: string) => IFinishSelectors;
    };
    bundleConfig: () => {
        menu: () => IFinishSelectors;
        link: (id: string) => IFinishSelectors;
    };
    user: () => {
        profile: () => IFinishSelectors;
        logout: () => IFinishSelectors;
    };
    search: () => {
        menu: () => IFinishSelectors;
        input: () => IFinishSelectors;
        firstResult: () => IFinishSelectors;
        results: () => IFinishSelectors;
    };
    spaceTree: () => {
        menu: () => IFinishSelectors;
    };
    customize: () => {
        menu: () => IFinishSelectors;
        pageEdit: () => IFinishSelectors;
        componentEdit: (name: any) => IFinishSelectors;
    };
}

interface IFinishSelectors {
    get: (options?: SelectorOptions | undefined) => Selector;
    getString: (options?: SelectorOptions | undefined) => string;
}
