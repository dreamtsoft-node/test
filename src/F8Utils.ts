import { ClientFunction } from 'testcafe';
import F8Auth from './F8Auth';
import { F8Select } from './index';

const DEFAULT_LOCAL_DEVELOPMENT_BASE_URL = 'http://localhost:8080';
const DEFAULT_LOCAL_USER_NAME = 'admin';
const DEFAULT_LOCAL_PASSWORD = 'admin';

export function getBaseUrl(): string {
    return process.env.BASE_URL ? process.env.BASE_URL : DEFAULT_LOCAL_DEVELOPMENT_BASE_URL;
}

export function getSpaceId(): string | undefined {
    return process.env.SPACE_ID ? process.env.SPACE_ID : undefined;
}

export function URL(path: string): string {
    const BASE_URL = getBaseUrl();
    const SPACE_ID = getSpaceId();
    return SPACE_ID ? `${BASE_URL}/s/${SPACE_ID}${path}` : `${BASE_URL}${path}`;
}

export function loginCredentials(): { username: string; password: string } {
    return {
        username: process.env.E2E_USER ? process.env.E2E_USER : DEFAULT_LOCAL_USER_NAME,
        password: process.env.E2E_PASS ? process.env.E2E_PASS : DEFAULT_LOCAL_PASSWORD,
    };
}

export function currentUrl(): Promise<string> {
    return ClientFunction(() => window.location.href)();
}

export async function loginUser(t: TestController, username = loginCredentials().username, password = loginCredentials().password, loginUrl = URL('/')): Promise<void> {
    const BASE_URL = getBaseUrl();
    const loginUrlFinal = process.env.FORCE_LOGIN_PATH ? `${BASE_URL}${process.env.FORCE_LOGIN_PATH}` : loginUrl;

    await F8Auth.login(t, username, password, loginUrlFinal);
}

export async function openMenu(t: TestController) {
    const hoverOverMenu = F8Select()
        .component('left_bar', 'root')
        .get();
    const openButton = F8Select()
        .component('left_bar', 'open')
        .get();

    await t.hover(hoverOverMenu);

    if (await openButton.with({ boundTestRun: t }).exists) {
        await t.click(openButton);
    }
}

export async function openWorkspaceStatus(t: TestController) {
    await t.pressKey('alt+w');
}
