
npm-login:
	@npm login --scope=@dreamtsoft

pack-for-local-testing:
	@npm pack

publish-package:
	@npm publish --access public

increment-major-version:
	@npm version major

increment-minor-version:
	@npm version minor

increment-patch-version:
	@npm version patch
